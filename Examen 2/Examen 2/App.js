import React, { useState, useEffect } from 'react';
import { View, Text, ScrollView, StyleSheet } from 'react-native';
import FilterButton from './FilterButton';
import { fetchData } from './Api';

const App = () => {
  const [data, setData] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const [filterType, setFilterType] = useState('');

  useEffect(() => {
    fetchData('http://jsonplaceholder.typicode.com/todos').then(data => setData(data));
  }, []);

  const applyFilter = (type) => {
    setFilterType(type);
    let filtered = [];
    switch (type) {
      case 'allIds':
        filtered = data.map(item => ({ id: item.id }));
        break;
      case 'IdsAndTitles':
        filtered = data.map(item => ({ id: item.id, title: item.title }));
        break;
      case 'uncompletedIdsAndTitles':
        for (const item of data) {
          if (!item.completed) {
            filtered.push({ id: item.id, title: item.title });
          }
        }
        break;
      case 'completedIdsAndTitles':
        for (const item of data) {
          if (item.completed) {
            filtered.push({ id: item.id, title: item.title });
          }
        }
        break;
      case 'idsAndUserId':
        filtered = data.map(item => ({ id: item.id, userId: item.userId }));
        break;
      case 'completedIdsAndUserId':
        for (const item of data) {
          if (item.completed) {
            filtered.push({ id: item.id, userId: item.userId });
          }
        }
        break;
      case 'uncompletedIdsAndUserId':
        for (const item of data) {
          if (!item.completed) {
            filtered.push({ id: item.id, userId: item.userId });
          }
        }
        break;
      default:
        filtered = [];
        break;
    }
    setFilteredData(filtered);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>NFL PENDIENTES</Text>
      <View style={styles.buttonsContainer}>
        <FilterButton text="Solo IDs" onPress={() => applyFilter('allIds')} />
        <FilterButton text="IDs y Titles" onPress={() => applyFilter('IdsAndTitles')} />
        <FilterButton text="Sin resolver (ID y Title)" onPress={() => applyFilter('uncompletedIdsAndTitles')} />
        <FilterButton text="Resueltos (ID y Title)" onPress={() => applyFilter('completedIdsAndTitles')} />
        <FilterButton text="IDs y UserID" onPress={() => applyFilter('idsAndUserId')} />
        <FilterButton text="Resueltos (ID y UserID)" onPress={() => applyFilter('completedIdsAndUserId')} />
        <FilterButton text="Sin resolver (ID y UserID)" onPress={() => applyFilter('uncompletedIdsAndUserId')} />
      </View>
      <ScrollView style={styles.scrollView}>
        {filteredData.map((item, index) => (
          <View key={index} style={styles.dataItem}>
            {filterType.includes('Id') && <Text>{`ID: ${item.id}`}</Text>}
            {filterType.includes('Title') && <Text>{`Título: ${item.title}`}</Text>}
            {filterType.includes('User') && <Text>{`UserID: ${item.userId}`}</Text>}
          </View>
        ))}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: '#fff',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  buttonsContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginBottom: 20,
  },
  clearButton: {
    backgroundColor: 'red',
    padding: 10,
    borderRadius: 10,
    marginBottom: 10,
  },
  clearButtonText: {
    color: 'black',
    textAlign: 'center',
  },
  scrollView: {
    flex: 1,
  },
  dataItem: {
    marginBottom: 10,
    padding: 10,
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 5,
  },
});

export default App;
