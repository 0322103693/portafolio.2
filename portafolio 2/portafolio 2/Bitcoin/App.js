import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, View } from 'react-native';
import BtcUsd from './components/BtcUsd';
import BtcEuro from './components/BtcEuro';
import BtcGbp from './components/BtcGbp';

export default function App() {
  return (
    <View style={styles.container}>
      <BtcUsd />
      <BtcEuro />
      <BtcGbp />
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
