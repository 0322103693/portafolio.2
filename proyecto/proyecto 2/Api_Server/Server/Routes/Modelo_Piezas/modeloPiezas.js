const express = require('express');
const modeloPiezas = express.Router();

//--------------------------------------------------Ruta de prueba(consulta)

modeloPiezas.get('/', (req, res) => {
    req.getConnection((err, connection) => {
        if(err) return res.send(err)

        connection.query('SELECT * FROM modelopiezas', (err, rows) => {
            if (err) return res.send(err)

            res.json(rows)
        })
    })
});


//--------------------------------------------------Ruta de prueba(insertar)

modeloPiezas.post('/', (req, res) => {
    req.getConnection((err, connection) => {
        if(err) return res.send(err)

        connection.query('INSERT INTO modeloPiezas SET ?', [req.body], (err, rows) => {
            if(err) return res.send(err)

            res.send('Se agrego exitosamente')
        })
    })
});

//--------------------------------------------------Ruta de prueba(Eliminar)

modeloPiezas.delete('/:numero', (req, res) => {
    req.getConnection((err, connection) => {
        if(err) return res.send(err)

        connection.query('DELETE FROM modelopiezas WHERE numero = ?', [req.params.numero], (err, rows) =>{
            if(err) return res.send(err)

            res.send('Se elimino exitosamente')
        })
    })
});

//--------------------------------------------------Ruta de prueba(Actualizar)

modeloPiezas.put('/:numero', (req, res) => {
    req.getConnection((err, connection) => {
        if(err) return res.send(err)

        connection.query('UPDATE modelopiezas SET ? WHERE numero = ?', [req.body, req.params.numero], (err, rows) => {
            if(err) return res.send(err)

            res.send('Se actualizo exitosamente')
        })
    })
});

//---------------------------------------------------Exportar modulo
module.exports = modeloPiezas