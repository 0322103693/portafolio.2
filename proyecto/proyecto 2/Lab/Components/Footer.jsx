
import { View, Box } from "@gluestack-ui/themed";

export const Footer = ({color = "#2D3154"})  => {
    return (
        <View
        alignItems="flex-end"
    >
        <Box
            bg={color}
            height={60}
            width="$full"
            maxWidth="100%"
        />
    </View>
    );
}