import { Center, Text } from '@gluestack-ui/themed';
  import React from 'react';
  import { View, StyleSheet, Dimensions } from 'react-native';
  import { LineChart } from 'react-native-chart-kit';
  
  const screenWidth = Dimensions.get('window').width;
  
  // Configuración del gráfico

  
  export const GraphicCard = ({text, color = "#E45007"}) => {
    // Datos de ejemplo para la gráfica
    const chartData = {
      labels: ['Elemento 1', 'Elemento 2', 'Elemento 3', 'Elemento 4', 'Elemento 5'],
      datasets: [
        {
          data: [18, 26, 23, 35, 36],
        },
      ],
    };
    const chartConfig = {
        backgroundColor: 'transparent', // Fondo transparente
        backgroundGradientFromOpacity: 0,
        backgroundGradientToOpacity: 0,
        decimalPlaces: 0,
        color: () => color, // Color de las barras
        labelColor: () => '#000000', // Color de las etiquetas
        style: {
          borderRadius: 16, // Esquinas redondeadas de las barras
        },
        propsForBackgroundLines: {
          strokeWidth: 0.5, // Grosor de las líneas de fondo
          stroke: 'rgba(200, 200, 200, 0.5)', // Color gris claro
          strokeDasharray: [0],
          strokeOpacity: 0.7,
          
        },
      };
  
    return (
        <View>
            <Center w={360} h={250} bg="#FFFFFF" borderRadius={20} >
                <View style={styles.container}>
                    <View style={styles.chartContainer}>
                    <LineChart
                        data={chartData}
                        width={400} // Ancho de la gráfica al 100% del contenedor
                        height={220} // Altura de la gráfica al 100% del contenedor
                        chartConfig={chartConfig}
                        style={styles.chart}
                        fromZero
                    />
                    </View>
                </View>
            </Center>
            <Text fontSize={14} color='#3B1B0D' textAlign='center'>{text}</Text>
        </View>
          
    );
  };
  
  // Estilos
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#141D57', // Color de fondo del contenedor
    },
    chartContainer: {
      position: 'absolute', // Posición absoluta para superponer la gráfica
      top: 25,
      left: 0,
      right: 20,
      bottom: 0,
      alignItems: 'center',
      justifyContent: 'center',
    },
  });
  
  