import React, { useState } from "react";
import { View, Center, VStack, Button, ButtonText, Input, InputField, InputIcon, InputSlot, Box } from "@gluestack-ui/themed";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';


export const LoginForm = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleLogin = () => {
    console.log('Username:', username);
    console.log('Password:', password);
  };

  const handleForgotPassword = () => {
    console.log('Forgot Password clicked');
    // Aquí puedes agregar la lógica para manejar el olvido de contraseña
  };

  return (
      <Center w="$full" maxWidth={320} maxHeight={300} h="$2/3" px={20} py={20} bg="#FFFFFF" alignItems="center" borderRadius={25}>
        <VStack w="$full" space="3xl" alignItems="space-betwwen">

          <Input bgColor="#E8E8E8" variant="rounded"  isDisabled={false} isInvalid={false} isReadOnly={false}>
            <InputSlot alignItems="center">
              <InputIcon marginLeft={10}><MaterialCommunityIcons name="account-circle-outline" color="#FFFFFF" size={20}/></InputIcon>
            </InputSlot>
            <InputField placeholderTextColor="#C6C3C3"  placeholder='Email' onChangeText={(text) => setUsername(text)} />
          </Input>

          <VStack alignItems="space-betwwen">
            <Input bgColor="#E8E8E8" variant="rounded"  isDisabled={false} isInvalid={false} isReadOnly={false} >
              <InputSlot marginLeft={10}>
                <InputIcon><MaterialCommunityIcons name="lock-outline" color="#FFFFFF" size={20}/></InputIcon>
              </InputSlot>
              <InputField placeholderTextColor="#C6C3C3" placeholder='Password' secureTextEntry onChangeText={(text) => setPassword(text)} />
            </Input>
            
            <Button alignSelf="flex-end" size="xs" variant="link" onPress={handleForgotPassword}>
              <ButtonText color="#1D2872">Forgot password?</ButtonText>
            </Button>
          </VStack>

          <Button w="$full" borderRadius={25} alignSelf="center" variant="solid" bgColor="#4B7583" isDisabled={false} isFocusVisible={false} onClick={handleLogin}>
            <ButtonText>Login</ButtonText>
          </Button>

        </VStack>
      </Center>
  );
};

