import { Heading, Text  } from '@gluestack-ui/themed';

export const Title = ({text}) => {
    return (
        <Heading py="$5" px="$5">
            <Text color="#3B1B0D" fontSize={20} fontWeight="bold">{text}</Text>
        </Heading>
    );
}

export const Title2 = ({text}) => {
    return (
        
            <Text color="#FFFFFF" italic={false} size="6xl" >{text}</Text>
        
    );
}