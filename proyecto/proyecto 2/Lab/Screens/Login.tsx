import React from 'react';
import { StyleSheet, } from "react-native";
import { View, Center} from '@gluestack-ui/themed';
import { LoginForm, Title2, Screens } from "../Components/Main";
import {LinearGradient} from 'expo-linear-gradient';


const Login = () => {
  const screen = Screens();
  return (
    <View style={styles.container}>
      <LinearGradient
        colors={['#6D4B70', '#000000', '#3A313B', '#000000' ]}
        style={styles.linearGradient}
      >
            <Center w="$full" h="$full">
                
                <Title2 text="OPPEN" />
                
                <LoginForm onPress={screen.Main}/>
                
            </Center>
      </LinearGradient>
    </View>
    
  );
}

const styles = StyleSheet.create({
  linearGradient: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,

    justifyContent: 'center',
  },
  container: {
    flex: 1,
    justifyContent: 'space-between',
    backgroundColor: 'transparent'
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
});

export default Login;


